# TheiaOnOCI

Eclipse Theia ist eine erweiterbare Cloud IDE, um sprachunabhängig entwickeln zu können. Hier das (cloud-init) Skript, um es in der Oracle Cloud Infrastructure (OCI) in einem Oracle Linux 8.x VM-Image aufzusetzen.

Installations-Dir: /opt/theia und /opt/node
Installations-Log: /home/opc/theia_setup.log

Nach erfolgreicher Installation kann man eine SSH Tunnel-Weiterleitung (z.B. in Putty) für Port 3000 konfigurieren und die Theia Webkonsole über http://localhost:3000 aufrufen.