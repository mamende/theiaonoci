#!/bin/bash

export home=/home/opc

(

# Install Git
echo "Install Git"
sudo yum install git -y
git --version

# Clone Theia
sudo mkdir /opt/theia
sudo git clone https://github.com/eclipse-theia/theia /opt/theia

# Install Node 12.14.1
wget https://nodejs.org/download/release/v12.14.1/node-v12.14.1-linux-x64.tar.gz
sudo mkdir /opt/node
sudo tar xvzf node-v12.14.1-linux-x64.tar.gz -C /opt/node
sudo rm node-v12.14.1-linux-x64.tar.gz
 
# Set environment 
export NODE_HOME=/opt/node/node-v12.14.1-linux-x64 
export PATH=$PATH:$NODE_HOME/bin 
echo 'export NODE_HOME=/opt/node/node-v12.14.1-linux-x64' >>$home/.bash_profile
echo 'export PATH=$PATH:$NODE_HOME/bin' >>$home/.bash_profile
 
# Install Yarn
curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo
curl --silent --location https://rpm.nodesource.com/setup_12.x | sudo bash -

sudo yum install yarn -y
yarn --version

# Update OL8
sudo dnf update -y

# Enable Oracle Linux 8 CodeReady Builder (x86_64) - Unsupported Repo
sudo yum-config-manager --enable ol8_codeready_builder -y
sudo dnf repolist all

# Install additional packages (dev tools and key mapper)
sudo yum install gcc gcc-c++ make -y
sudo yum install libX11-devel libxkbfile-devel -y

# Build Theia
cd /opt/theia/
sudo yarn

# Theia autostart
touch theia
echo '#!/bin/sh' >>theia
echo "# chkconfig: 345 99 10" >>theia
echo "# description: Theia autostart." >>theia
echo 'case "$1" in' >>theia
echo "  'start')" >>theia
echo "    cd /opt/theia/examples/browser" >>theia
echo "    (sudo -u opc yarn run start) |& sudo -u opc tee $home/theia.log" >>theia
echo "  ;;" >>theia
echo "  'stop')" >>theia
echo "  ;;" >>theia
echo "esac" >>theia
sudo chmod 750 theia
sudo mv theia /etc/init.d/
sudo chkconfig --add theia

# Start Theia
cd examples/browser
(sudo -u opc yarn run start) |& sudo -u opc tee $home/theia.log

) |& sudo -u opc tee $home/theia_setup.log



